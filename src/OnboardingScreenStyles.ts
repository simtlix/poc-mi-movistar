import {StyleSheet} from 'react-native';
import {defaultColors} from './common/styles';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: defaultColors.WHITE,
  },
  image: {
    flex: 1,
    justifyContent: 'space-between',
    opacity: 0.9,
  },
  logo: {
    alignSelf: 'center',
    width: 230,
    height: 230,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: defaultColors.BLACK,
    opacity: 0.5,
  },
  text: {
    textAlign: 'center',
    color: defaultColors.WHITE,
  },
  title: {
    fontSize: 16,
    bottom: 5,
  },
  description: {
    fontSize: 12,
  },
  textBold: {
    fontWeight: 'bold',
  },
  termsAndCond: {
    top: 20,
    fontSize: 12,
    maxWidth: 300,
    alignSelf: 'center',
  },
  content: {
    bottom: '10%',
  },
  button: {
    alignSelf: 'center',
    width: '75%',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flexBasis: 40,
    marginTop: 15,
  },
  enterButton: {
    backgroundColor: defaultColors.MOVISTAR_GREEN,
  },
  portableButton: {
    backgroundColor: 'transparent',
    borderWidth: 0.5,
    borderColor: defaultColors.WHITE,
  },
});

export default styles;
