const config = {
  PORTAL_URL: 'https://web.movistar.com.ar',
  IDP_URL: 'https://idp.movistar.com.ar',
  API_URL: 'https://api-ar.mytelco.io',
  LOGIN_PATH: '/pages/open-id',
  INIT_PATH: '/consumptions',
  TOKEN_PATH: '/token',
};

export {config};
