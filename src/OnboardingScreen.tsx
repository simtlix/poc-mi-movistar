import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  TouchableOpacity,
  Text,
  Image,
} from 'react-native';
import {StackNavigationProp} from '@react-navigation/stack';
import {
  OnboardingStackParamList,
  OnboardingStackRoutes,
} from './OnboardingRoutes';
import {ApplicationState} from './common/definitions';
import {AuthenticationDispatchProps} from './authentication/redux/definitions';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import * as authActions from './authentication/redux/actions';
import styles from './OnboardingScreenStyles';

type OwnProps = {
  navigation: StackNavigationProp<OnboardingStackParamList>;
};
interface StateProps {}

type OnboardingScreenProps = OwnProps &
  StateProps &
  AuthenticationDispatchProps;

class OnboardingScreen extends Component<OnboardingScreenProps> {
  public render() {
    const {navigation} = this.props;
    const image = require('../assets/splash_bg.jpg');
    const logo = require('../assets/brand_icon_white.png');
    const navigateToLogin = () => {
      navigation.navigate(OnboardingStackRoutes.LOGIN);
    };

    return (
      <View style={styles.container}>
        <ImageBackground source={image} style={styles.image}>
          <View style={styles.overlay} />
          <Image style={styles.logo} source={logo} />
          <View style={styles.content}>
            <Text style={[styles.text, styles.textBold, styles.title]}>
              Tu línea siempre a mano
            </Text>
            <Text style={[styles.text, styles.description]}>
              Lo que necesites para gestionar tu cuenta está acá
            </Text>
            <TouchableOpacity
              style={[styles.button, styles.enterButton]}
              onPress={navigateToLogin}>
              <Text style={[styles.text, styles.textBold]}>
                {'Ingresa ahora'}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.button, styles.portableButton]}
              onPress={() => console.log('button: porta tu linea')}>
              <Text style={[styles.text, styles.textBold]}>
                {'Portá tu línea'}
              </Text>
            </TouchableOpacity>
            <Text style={[styles.text, styles.termsAndCond]}>
              Al continuar has leído y aceptás nuestros términos y condiciones
            </Text>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

function mapStateToProps(state: ApplicationState): StateProps {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch): AuthenticationDispatchProps {
  return {
    authenticationActions: bindActionCreators({...authActions}, dispatch),
  };
}

export default connect<
  StateProps,
  AuthenticationDispatchProps,
  OwnProps,
  ApplicationState
>(
  mapStateToProps,
  mapDispatchToProps,
)(OnboardingScreen);
