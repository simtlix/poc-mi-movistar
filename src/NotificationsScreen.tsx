import React, {FunctionComponent} from 'react';
import {View, Text} from 'react-native';
import {globalStyles} from './common/styles';

const NotificationsScreen: FunctionComponent = ({}) => {
  return (
    <View>
      <Text style={globalStyles.screenTitle}>Notificaciones</Text>
    </View>
  );
};

export default NotificationsScreen;
