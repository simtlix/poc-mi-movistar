import React, {FunctionComponent} from 'react';
import {View, Text} from 'react-native';
import {globalStyles} from './common/styles';

const DiscoverScreen: FunctionComponent = ({}) => {
  return (
    <View>
      <Text style={globalStyles.screenTitle}>Descubrí</Text>
    </View>
  );
};

export default DiscoverScreen;
