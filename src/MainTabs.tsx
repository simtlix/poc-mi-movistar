import React, {FunctionComponent} from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/EvilIcons';
import AccountScreen from './AccountScreen';
import DiscoverScreen from './DiscoverScreen';
import NotificationsScreen from './NotificationsScreen';
import SettingsScreen from './SettingsScreen';
import {defaultColors} from './common/styles';

enum MainTabsRoutes {
  ACCOUNT = 'Cuenta',
  DISCOVER = 'Descrubrí',
  NOTIFICATIONS = 'Notificaciones',
  SETTINGS = 'Ajustes',
}

type MainTabsParamList = {
  [MainTabsRoutes.ACCOUNT]: undefined;
  [MainTabsRoutes.DISCOVER]: undefined;
  [MainTabsRoutes.NOTIFICATIONS]: undefined;
  [MainTabsRoutes.SETTINGS]: undefined;
};

const tabIcons = {
  [MainTabsRoutes.ACCOUNT]: 'chart',
  [MainTabsRoutes.DISCOVER]: 'eye',
  [MainTabsRoutes.NOTIFICATIONS]: 'comment',
  [MainTabsRoutes.SETTINGS]: 'gear',
};

const MainTabs: FunctionComponent = ({}) => {
  const Tab = createBottomTabNavigator<MainTabsParamList>();
  return (
    <Tab.Navigator
      backBehavior={'initialRoute'}
      initialRouteName={MainTabsRoutes.ACCOUNT}
      screenOptions={({route}) => ({
        tabBarIcon: ({color, size}) => {
          const iconName = tabIcons[route.name];
          return <Ionicons name={iconName} size={size} color={color} />;
        },
      })}
      tabBarOptions={{
        activeTintColor: defaultColors.MOVISTAR_BLUE,
        inactiveBackgroundColor: defaultColors.WHITE,
      }}>
      <Tab.Screen name={MainTabsRoutes.ACCOUNT} component={AccountScreen} />
      <Tab.Screen name={MainTabsRoutes.DISCOVER} component={DiscoverScreen} />
      <Tab.Screen
        name={MainTabsRoutes.NOTIFICATIONS}
        component={NotificationsScreen}
      />
      <Tab.Screen name={MainTabsRoutes.SETTINGS} component={SettingsScreen} />
    </Tab.Navigator>
  );
};

export default MainTabs;
