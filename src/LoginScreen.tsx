import React, {Component} from 'react';
import WebView from 'react-native-webview';
import {Text} from 'react-native';
import {config} from './config';
import {connect} from 'react-redux';
import {Dispatch, bindActionCreators} from 'redux';
import {AuthenticationDispatchProps} from './authentication/redux/definitions';
import {ApplicationState} from './common/definitions';
import * as authActions from './authentication/redux/actions';
import {WebViewMessageEvent} from 'react-native-webview/lib/WebViewTypes';
import CookieManager from '@react-native-community/cookies';
import {AUTHENTICATION_COOKIE_SID} from './common/constants';
import {globalStyles} from './common/styles';

interface OwnProps {}
interface StateProps {}
type LoginScreenProps = OwnProps & StateProps & AuthenticationDispatchProps;

class LoginScreen extends Component<LoginScreenProps> {
  webview: WebView = null;
  intervalId: NodeJS.Timeout = null;

  public render() {
    const {
      authenticationActions: {signInAction},
    } = this.props;
    const webViewUrl = config.PORTAL_URL + config.LOGIN_PATH;
    const onNavigationStateChange = () => {
      if (this.intervalId != null) {
        clearInterval(this.intervalId);
      }
      const checkSidCookie = async () => {
        const cookies = await CookieManager.get(config.PORTAL_URL);
        const sid = cookies[AUTHENTICATION_COOKIE_SID];
        if (sid === undefined) {
          console.log('sid cookie not available yet, retrying...');
        } else {
          console.log('completing sign in...');
          signInAction('refreshToken');
          clearInterval(this.intervalId);
        }
      };
      this.intervalId = setInterval(checkSidCookie, 500);
    };

    return (
      <React.Fragment>
        <Text style={globalStyles.screenTitle}>Login</Text>
        <WebView
          ref={ref => (this.webview = ref)}
          originWhitelist={['*']}
          source={{uri: webViewUrl}}
          sharedCookiesEnabled={true}
          style={{marginTop: 0}}
          onNavigationStateChange={onNavigationStateChange}
        />
      </React.Fragment>
    );
  }

  public componentWillUnmount() {
    clearInterval(this.intervalId);
  }
}

function mapStateToProps(): StateProps {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch): AuthenticationDispatchProps {
  return {
    authenticationActions: bindActionCreators({...authActions}, dispatch),
  };
}

export default connect<
  StateProps,
  AuthenticationDispatchProps,
  OwnProps,
  ApplicationState
>(
  mapStateToProps,
  mapDispatchToProps,
)(LoginScreen);
