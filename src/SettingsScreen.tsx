import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {AuthenticationDispatchProps} from './authentication/redux/definitions';
import {ApplicationState} from './common/definitions';
import {connect} from 'react-redux';
import {bindActionCreators, Dispatch} from 'redux';
import * as authActions from './authentication/redux/actions';
import {ConfirmDialog} from 'react-native-simple-dialogs';
import styles from './SettingsScreenStyles';
import {globalStyles} from './common/styles';

interface OwnProps {}
interface StateProps {
  showConfirm: boolean;
}
type SettingsScreenProps = OwnProps & StateProps & AuthenticationDispatchProps;
class SettingsScreen extends Component<SettingsScreenProps> {
  public render() {
    const {showConfirm} = this.props;
    const {
      signOutAction,
      signOutShowConfirmAction,
      signOutHideConfirmAction,
    } = this.props.authenticationActions;
    const showConfirmDialog = () => {
      signOutShowConfirmAction();
    };
    const cancel = () => {
      signOutHideConfirmAction();
    };
    const confirm = async () => {
      signOutAction();
    };
    return (
      <View>
        <Text style={globalStyles.screenTitle}>Ajustes</Text>
        <ConfirmDialog
          title="Si cierras la sesión, no recibirás ninguna notificación"
          message=""
          visible={showConfirm}
          onTouchOutside={cancel}
          positiveButton={{
            title: 'Cerrar sesión',
            onPress: confirm,
          }}
          negativeButton={{
            title: 'Cancelar',
            onPress: cancel,
          }}
        />
        <TouchableOpacity
          style={[styles.listButton]}
          onPress={showConfirmDialog}>
          <Text style={[styles.listButtonText]}>Cerrar sesión</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

function mapStateToProps(state: ApplicationState): StateProps {
  return {
    showConfirm: state.authentication.showConfirm,
  };
}

function mapDispatchToProps(dispatch: Dispatch): AuthenticationDispatchProps {
  return {
    authenticationActions: bindActionCreators({...authActions}, dispatch),
  };
}

export default connect<
  StateProps,
  AuthenticationDispatchProps,
  OwnProps,
  ApplicationState
>(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsScreen);
