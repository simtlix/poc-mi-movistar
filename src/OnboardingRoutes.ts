export enum OnboardingStackRoutes {
  ONBOARDING = 'ONBOARDING',
  LOGIN = 'LOGIN',
}

export type OnboardingStackParamList = {
  [OnboardingStackRoutes.ONBOARDING]: undefined;
  [OnboardingStackRoutes.LOGIN]: undefined;
};
