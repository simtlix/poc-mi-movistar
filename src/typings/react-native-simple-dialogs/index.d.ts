declare module 'react-native-simple-dialogs' {
  interface IConfirmDialogProps {
    title: string;
    message: string;
    visible: boolean;
    onTouchOutside: () => void;
    positiveButton: {
      title: string;
      onPress: () => Promise<void>;
    };
    negativeButton: {
      title: string;
      onPress: () => void;
    };
  }

  class ConfirmDialog extends React.Component<IConfirmDialogProps> {}
}
