import React, {Component} from 'react';
import {OnboardingStack} from './OnboardingStack';
import {connect} from 'react-redux';
import {AuthenticationDispatchProps} from './authentication/redux/definitions';
import {bindActionCreators, Dispatch} from 'redux';
import {ApplicationState} from './common/definitions';
import * as authActions from './authentication/redux/actions';
import MainTabs from './MainTabs';

interface OwnProps {}
interface StateProps {
  authenticated: boolean;
}
type AuthenticationCheckerProps = OwnProps &
  StateProps &
  AuthenticationDispatchProps;
class AuthenticationChecker extends Component<AuthenticationCheckerProps> {
  public render() {
    const {authenticated} = this.props;
    if (!authenticated) {
      return <OnboardingStack />;
    } else {
      return <MainTabs />;
    }
  }
}

function mapStateToProps(state: ApplicationState): StateProps {
  return {
    authenticated: state.authentication.authenticated,
  };
}

function mapDispatchToProps(dispatch: Dispatch): AuthenticationDispatchProps {
  return {
    authenticationActions: bindActionCreators({...authActions}, dispatch),
  };
}

export default connect<
  StateProps,
  AuthenticationDispatchProps,
  OwnProps,
  ApplicationState
>(
  mapStateToProps,
  mapDispatchToProps,
)(AuthenticationChecker);
