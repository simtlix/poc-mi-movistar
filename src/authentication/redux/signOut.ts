import {Dispatch} from 'react';
import {
  AUTH_SIGN_OUT_BEGIN,
  AUTH_SIGN_OUT_SUCCESS,
  AUTH_SIGN_OUT_FAILURE,
  AUTH_SIGN_OUT_SHOW_CONFIRM,
  AUTH_SIGN_OUT_HIDE_CONFIRM,
} from './constants';
import {
  AuthenticationState,
  SignOutAction,
  SignOutDispatchAction,
  SignOutActionResponse,
  SignOutShowConfirmAction,
  SignOutHideConfirmAction,
} from './definitions';
import {config} from './../../config';
import axios from 'axios';
import CookieManager from '@react-native-community/cookies';
import AsyncStorage from '@react-native-community/async-storage';
import {AUTHENTICATION_REFRESH_TOKEN} from '../../common/constants';
import {AnyAction} from 'redux';

const signOutShowConfirmAction: SignOutShowConfirmAction = () => {
  return (dispatch: Dispatch<AnyAction>) => {
    dispatch({
      type: AUTH_SIGN_OUT_SHOW_CONFIRM,
    });
  };
};

const signOutHideConfirmAction: SignOutHideConfirmAction = () => {
  return (dispatch: Dispatch<AnyAction>) => {
    dispatch({
      type: AUTH_SIGN_OUT_HIDE_CONFIRM,
    });
  };
};

const signOutAction: SignOutAction = () => {
  return (dispatch: Dispatch<SignOutDispatchAction>) => {
    dispatch({
      type: AUTH_SIGN_OUT_BEGIN,
    });
    const promise = new Promise<SignOutActionResponse>(
      async (resolve, reject) => {
        try {
          // perform real signout
          const responseData = await doSignOut();
          dispatch({
            type: AUTH_SIGN_OUT_SUCCESS,
            data: responseData,
          });
          resolve(responseData);
        } catch (error) {
          dispatch({
            type: AUTH_SIGN_OUT_FAILURE,
            data: error,
          });
          reject(error);
        }
      },
    );
    return promise;
  };
};

const doSignOut = async (): Promise<SignOutActionResponse> => {
  const boundary = '----WebKitFormBoundary';
  const client = axios.create({
    baseURL: config.API_URL,
    headers: {'Content-Type': `multipart/form-data; boundary=${boundary}`},
  });
  const cookies = await CookieManager.get(config.PORTAL_URL);
  const sid = cookies['sid'];
  const webiid = cookies['webiid'];
  await CookieManager.clearAll();
  await AsyncStorage.removeItem(AUTHENTICATION_REFRESH_TOKEN);
  const body = `--${boundary}
Content-Disposition: form-data; name="request"

{"jsonrpc":"2.0","method":"Auth.3.closeSession","params":{"installationId":"${webiid}"},"id":12}
--${boundary}
Content-Disposition: form-data; name="x-tuenti-apicontext"

{"installationId":"${webiid}","screenSize":"xhdpi","appBrand":"MovistarAR","webappVersion":"942.0.0.ktlo.8"}
--${boundary}
Content-Disposition: form-data; name="authorization"

Bearer ${sid}
--${boundary}--`;
  try {
    const res = await client.post('/', body);
    console.log(`axios POST result: ${res}`);
  } catch (err) {
    console.log(`axios POST error: ${err}`);
  }
  return {};
};

function signOutReducer(
  state: AuthenticationState,
  action: SignOutDispatchAction,
): AuthenticationState {
  switch (action.type) {
    case AUTH_SIGN_OUT_BEGIN:
      return {
        ...state,
        signOutPending: true,
      };
    case AUTH_SIGN_OUT_FAILURE:
      return {
        ...state,
        signOutPending: false,
      };
    case AUTH_SIGN_OUT_SUCCESS:
      return {
        ...state,
        authenticated: false,
        signOutPending: false,
        showConfirm: false,
        refreshToken: '',
      };
    case AUTH_SIGN_OUT_SHOW_CONFIRM:
      return {
        ...state,
        showConfirm: true,
      };
    case AUTH_SIGN_OUT_HIDE_CONFIRM:
      return {
        ...state,
        showConfirm: false,
      };
    default:
      return state;
  }
}

export {
  signOutAction,
  signOutShowConfirmAction,
  signOutHideConfirmAction,
  signOutReducer,
};
