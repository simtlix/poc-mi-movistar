import {Dispatch} from 'react';
import {AUTH_SIGN_IN_COMPLETE} from './constants';
import {
  AuthenticationState,
  SignInAction,
  SignInDispatchAction,
  SignInResponse,
} from './definitions';
import AsyncStorage from '@react-native-community/async-storage';
import {AUTHENTICATION_REFRESH_TOKEN} from '../../common/constants';

const signInAction: SignInAction = (refreshToken: string) => {
  return (dispatch: Dispatch<SignInDispatchAction>) => {
    const promise = new Promise<void>(async resolve => {
      await AsyncStorage.setItem(AUTHENTICATION_REFRESH_TOKEN, refreshToken);
      const responseData: SignInResponse = {refreshToken};
      dispatch({
        type: AUTH_SIGN_IN_COMPLETE,
        data: responseData,
      });
      resolve();
    });
    return promise;
  };
};

function signInReducer(
  state: AuthenticationState,
  action: any,
): AuthenticationState {
  switch (action.type) {
    case AUTH_SIGN_IN_COMPLETE:
      return {
        ...state,
        authenticated: true,
        refreshToken: action.data.refreshToken,
        signOutPending: false,
      };

    default:
      return state;
  }
}

export {signInAction, signInReducer};
