import {Dispatch} from 'react';
import {SuccessDispatchAction} from '../../common/definitions';
import {AnyAction} from 'redux';

/** Authentication state */
export interface AuthenticationState {
  authenticated: boolean;
  signOutPending: boolean;
  showConfirm: boolean;
  refreshToken: string;
}

/** SignIn definitions */
export interface SignInResponse {
  refreshToken: string;
}
export type SignInDispatchAction = SuccessDispatchAction<SignInResponse>;
export type SignInAction = (
  refreshToken: string,
) => (dispatch: Dispatch<SignInDispatchAction>) => Promise<void>;

/** SignOut definitions */
export interface SignOutActionResponse {}
export type SignOutDispatchAction = SuccessDispatchAction<
  SignOutActionResponse
>;
export type SignOutAction = () => (
  dispatch: Dispatch<SignOutDispatchAction>,
) => Promise<SignOutActionResponse>;

/** SignOut show/hide confirm*/
export type SignOutShowConfirmAction = () => (
  dispatch: Dispatch<AnyAction>,
) => void;
export type SignOutHideConfirmAction = () => (
  dispatch: Dispatch<AnyAction>,
) => void;

/** Authentication dispatch */
export interface AuthenticationDispatchProps {
  authenticationActions: {
    signInAction: SignInAction;
    signOutAction: SignOutAction;
    signOutShowConfirmAction: SignOutShowConfirmAction;
    signOutHideConfirmAction: SignOutHideConfirmAction;
  };
}
