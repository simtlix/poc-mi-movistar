import {AuthenticationState} from './definitions';

const initialState: AuthenticationState = {
  authenticated: false,
  signOutPending: false,
  showConfirm: false,
  refreshToken: '',
};

export default initialState;
