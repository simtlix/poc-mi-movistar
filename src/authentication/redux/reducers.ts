import {signInReducer} from './signIn';
import {signOutReducer} from './signOut';
import initialState from './initialState';
const reducers = [signInReducer, signOutReducer];

export default function authenticationReducer(
  state = initialState,
  action: any,
) {
  let newState;
  switch (action.type) {
    default:
      newState = state;
      break;
  }
  const result = reducers.reduce((s, r) => r(s, action), newState);
  return result;
}
