import {createStore, compose, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './rootReducer';
import {createLogger} from 'redux-logger';

const middlewares: any[] = [thunk];
let devToolsExtension = (f: any) => f;

if (process.env.NODE_ENV === 'development') {
  const logger = createLogger({
    collapsed: true,
    stateTransformer: (state: any) => {
      return state;
    },
  });
  middlewares.push(logger);

  // if (window.__REDUX_DEVTOOLS_EXTENSION__) {
  //   devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__();
  // }
}

export default function configStore(initialState: object) {
  const store = createStore(
    rootReducer,
    initialState,
    compose(
      applyMiddleware(...middlewares),
      devToolsExtension,
    ),
  );

  return store;
}
