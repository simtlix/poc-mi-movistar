import {AnyAction} from 'redux';
import {AuthenticationState} from '../authentication/redux/definitions';

export interface SuccessDispatchAction<T> extends AnyAction {
  data?: T;
}

export interface ApplicationState {
  authentication: AuthenticationState;
}
