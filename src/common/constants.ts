export const AUTHENTICATION_COOKIE_WEBIID = 'webiid';
export const AUTHENTICATION_COOKIE_SID = 'sid';
export const AUTHENTICATION_REFRESH_TOKEN = '@refreshToken';
