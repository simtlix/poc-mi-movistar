import {combineReducers} from 'redux';
import authenticationReducer from '../authentication/redux/reducers';

const reducerMap = {
  authentication: authenticationReducer,
};

export default combineReducers(reducerMap);
