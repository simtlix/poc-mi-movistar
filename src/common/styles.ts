import {StyleSheet} from 'react-native';

export const defaultColors = {
  BLACK: '#000000',
  WHITE: '#FFFFFF',
  GREY_0: '#2A2A3C',
  GREY_1: '#757575',
  GREY_2: '#999999',
  GREY_3: '#DDDDDD',
  GREY_4: '#EEEEEE',
  GREY_5: '#F6F6F6',
  MOVISTAR_BLUE: '#00A9E0',
  MOVISTAR_BLUE_DARK: '#0098D2',
  MOVISTAR_BLUE_LIGHT_50: '#7FD4EF',
  MOVISTAR_BLUE_LIGHT_60: '#66CBEC',
  MOVISTAR_BLUE_LIGHT_60_P: '#D9F2F8',
  MOVISTAR_BLUE_LIGHT_30: '#B2E5F5',
  MOVISTAR_BLUE_LIGHT_30_P: '#ECF8FC',
  MOVISTAR_GREEN: '#5BC500',
  MOVISTAR_GREEN_DARK: '#499E00',
  MOVISTAR_GREEN_LIGHT_50: '#ADE27F',
  MOVISTAR_GREEN_LIGHT_30: '#CDEDB2',
  PEPPER: '#FF374A',
  PEPPER_LIGHT: '#FFC3C8',
  PEPPER_DARK: '#D73241',
  PINK: '#E9426D',
  PURPLE: '#954b97',
  EGG: '#F59C00',
  EGG_LIGHT: '#FCE1B2',
  MOVISTAR_PRIORITY: '#002D45',
};

export const globalStyles = StyleSheet.create({
  screenTitle: {
    textAlign: 'center',
    color: defaultColors.WHITE,
    paddingTop: 10,
    paddingBottom: 10,
    fontSize: 16,
    backgroundColor: defaultColors.MOVISTAR_BLUE,
  },
  refreshButton: {
    fontSize: 30,
    position: 'absolute',
    paddingTop: 10,
    right: 0,
    color: defaultColors.WHITE,
  },
});
