import React, {Component, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import AuthenticationChecker from './AuthenticationChecker';
import {Provider} from 'react-redux';
import configStore from './common/configStore';
import {View, Text} from 'react-native';
import CookieManager, {Cookie} from '@react-native-community/cookies';
import {config} from './config';
import SplashScreen from 'react-native-splash-screen';
import {ApplicationState} from './common/definitions';
import AsyncStorage from '@react-native-community/async-storage';
import {
  AUTHENTICATION_COOKIE_WEBIID,
  AUTHENTICATION_COOKIE_SID,
  AUTHENTICATION_REFRESH_TOKEN,
} from './common/constants';
import initialState from './authentication/redux/initialState';
import axios, {AxiosResponse} from 'axios';
import {stringify} from 'querystring';

interface AppState {
  loading: boolean;
}

let store: any = {};

class App extends Component<{}, AppState> {
  constructor(props: {}) {
    super(props);
    this.state = {loading: true};
  }

  componentDidMount() {
    this.loadApp();
  }

  private async loadApp() {
    // load initial state from storage
    const cookies = await CookieManager.get(config.PORTAL_URL);
    const sid = cookies[AUTHENTICATION_COOKIE_SID];
    const authenticated = sid !== undefined && sid.length > 0;
    const storeInitialState: ApplicationState = {
      authentication: {
        ...initialState,
        authenticated,
      },
    };
    store = configStore(storeInitialState);
    this.setState({
      loading: false,
    });
    SplashScreen.hide();
  }

  public render() {
    if (this.state.loading) {
      return (
        <View>
          <Text />
        </View>
      );
    }

    return (
      <Provider store={store}>
        <NavigationContainer>
          <AuthenticationChecker />
        </NavigationContainer>
      </Provider>
    );
  }

  private async loadResourcesAsync() {
    // await Font.loadAsync({
    // load font resources here
    // });
    const images = [
      require('../assets/splash_bg.jpg'),
      require('../assets/splash_bg_2x.jpg'),
      require('../assets/splash_bg_3x.jpg'),
    ];
    const cacheImages = images.map(image => {
      // return Asset.fromModule(image).downloadAsync();
    });
    await Promise.all(cacheImages);
  }
}

export default App;
