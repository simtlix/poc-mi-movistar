import {StyleSheet} from 'react-native';
import {defaultColors} from './common/styles';

const styles = StyleSheet.create({
  listButtonText: {
    textAlign: 'center',
    color: defaultColors.BLACK,
    padding: 10,
    fontSize: 16,
  },
  listButton: {
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: defaultColors.GREY_3,
  },
});

export default styles;
