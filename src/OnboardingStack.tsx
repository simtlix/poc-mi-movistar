import React, {FunctionComponent} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from './LoginScreen';
import OnboardingScreen from './OnboardingScreen';
import {
  OnboardingStackParamList,
  OnboardingStackRoutes,
} from './OnboardingRoutes';

export const OnboardingStack: FunctionComponent = ({}) => {
  const Stack = createStackNavigator<OnboardingStackParamList>();
  return (
    <Stack.Navigator
      initialRouteName={OnboardingStackRoutes.ONBOARDING}
      headerMode="none">
      <Stack.Screen
        name={OnboardingStackRoutes.ONBOARDING}
        component={OnboardingScreen}
      />
      <Stack.Screen
        name={OnboardingStackRoutes.LOGIN}
        component={LoginScreen}
      />
    </Stack.Navigator>
  );
};
