import React, {Component} from 'react';
import WebView, {WebViewNavigation} from 'react-native-webview';
import {Text, View} from 'react-native';
import Ionicons from 'react-native-vector-icons/EvilIcons';
import {config} from './config';
import {connect} from 'react-redux';
import {Dispatch, bindActionCreators} from 'redux';
import {AuthenticationDispatchProps} from './authentication/redux/definitions';
import {ApplicationState} from './common/definitions';
import * as authActions from './authentication/redux/actions';
import {globalStyles} from './common/styles';

interface OwnProps {}
interface StateProps {}
type PortalScreenProps = OwnProps & StateProps & AuthenticationDispatchProps;

class PortalScreen extends Component<PortalScreenProps> {
  webview: WebView = null;
  public render() {
    const onNavigationStateChange = (navigationEvent: WebViewNavigation) => {
      const {url} = navigationEvent;
      if (!url) {
        return;
      }
    };
    return (
      <React.Fragment>
        <View>
          <Text style={globalStyles.screenTitle}>Mi Movistar</Text>
          <Ionicons
            name="refresh"
            style={globalStyles.refreshButton}
            onPress={() => this.webview.reload()}
          />
        </View>
        <WebView
          ref={ref => (this.webview = ref)}
          originWhitelist={['*']}
          source={{uri: config.PORTAL_URL}}
          sharedCookiesEnabled={true}
          style={{marginTop: 0}}
          onNavigationStateChange={onNavigationStateChange}
        />
      </React.Fragment>
    );
  }
}

function mapStateToProps(state: ApplicationState): StateProps {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch): AuthenticationDispatchProps {
  return {
    authenticationActions: bindActionCreators({...authActions}, dispatch),
  };
}

export default connect<
  StateProps,
  AuthenticationDispatchProps,
  OwnProps,
  ApplicationState
>(
  mapStateToProps,
  mapDispatchToProps,
)(PortalScreen);
